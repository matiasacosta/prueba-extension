import * as vscode from 'vscode';
import { Tarjeta } from './tarjeta';

export class TarjetaProvider implements vscode.TreeDataProvider<Tarjeta>{

    private _onDidChangeTreeData: vscode.EventEmitter<Tarjeta | undefined> = new vscode.EventEmitter<Tarjeta | undefined>();
	readonly onDidChangeTreeData: vscode.Event<Tarjeta | undefined> = this._onDidChangeTreeData.event;
    tarjetas: Tarjeta[] = [];


	refresh(tarjeta?: Tarjeta): void {
        this._onDidChangeTreeData.fire(tarjeta);
	}
    
    setElementos(tarjetas: object[], listaId: string, listaNombre: string){
        this.tarjetas = this.crearTarjetasItem(tarjetas, listaId, listaNombre);
    }

    setElementosNull(){
        let tarjetas: Tarjeta[] = [];
        this.tarjetas = tarjetas;
    }

    getTreeItem(element: Tarjeta): vscode.TreeItem {
        return element;
    }

    getChildren(element?: Tarjeta): vscode.ProviderResult<Tarjeta[]>{
        if (element){ // Soy hijo
                return Promise.resolve([]);
        }else{ // Soy nodo raiz
            return Promise.resolve(this.tarjetas);
        }
    }

    crearTarjetasItem(tarjetas: object[], listaId: string, listaNombre: string): Tarjeta[] {
        let itemsTarjetas: Tarjeta[] = [];
        for (let tarjeta of tarjetas){
            let t = new Tarjeta(tarjeta.id, tarjeta.name, tarjeta.idBoard, listaId, listaNombre, vscode.TreeItemCollapsibleState.None);
            itemsTarjetas.push(t);
        }
        return itemsTarjetas;
    }

    getTarjeta(tarjetaId:string){
        return this.tarjetas.find((tarjeta) => {
            return tarjeta.id === tarjetaId;
        });
    }
}