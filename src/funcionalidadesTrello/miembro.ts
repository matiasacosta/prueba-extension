export class Miembro {
    
    public id: string;
    public username: string;
    public fullName: string;
    public email: string;
    public avatarUrl: string;

    constructor(datos: 
        {id: string,
        username: string,
        fullName:string,
        email: string,
        avatarUrl:string}){
        this.id = datos.id;
        this.username = datos.username;
        this.fullName = datos.fullName;
        this.email = datos.email;
        this.avatarUrl = datos.avatarUrl;
    }

}