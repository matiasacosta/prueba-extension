import * as vscode from 'vscode';
import { Tablero } from './tablero';
import { Lista } from './lista';

export class TableroProvider implements vscode.TreeDataProvider<Tablero>{

    private _onDidChangeTreeData: vscode.EventEmitter<Tablero | Lista | undefined> = new vscode.EventEmitter<Tablero | Lista | undefined>();
	readonly onDidChangeTreeData: vscode.Event<Tablero | Lista | undefined> = this._onDidChangeTreeData.event;
    tableros: Tablero[] = [];


	refresh(elemento?: Tablero | Lista): void {
        this._onDidChangeTreeData.fire(elemento);
	}

    setTableros(tableros: object[]){
        this.tableros = this.crearTablerosItems(tableros);
    }

    getTreeItem(element: Tablero | Lista): vscode.TreeItem {
        return element;
    }

    getChildren(element?: Tablero | Lista): vscode.ProviderResult<Tablero[]> | vscode.ProviderResult<Lista[]>{
        if (element){ // Soy hijo
            if(element.contextValue === 'tablero'){
                let tablero = this.tableros.find(function(t){
                    return t.label === element.label;
                });
                return Promise.resolve(tablero.listas);
            }else{
                return Promise.resolve([]);
            }
        }else{ // Soy nodo raiz
            return Promise.resolve(this.tableros);
        }
    }

    crearTablerosItems(tableros: object[]): Tablero[] {
        let itemsTableros: Tablero[] = [];        
        for (let tablero of tableros){
            let t = new Tablero(tablero.id, tablero.name, vscode.TreeItemCollapsibleState.Collapsed, tablero.lists);
            itemsTableros.push(t);
        }
        return itemsTableros;
    }
}