import axios, { AxiosPromise } from 'axios';
import { window } from 'vscode';

export class Cliente {
    public username: string | undefined;
    public key: string | undefined;
    public token: string | undefined;
    

    autenticarse(credenciales: string){
        var splitted = credenciales.split("\n", 3); 
             
        this.key = splitted[0];
        this.token = splitted[1];
        this.username = splitted[2];
        return 0;
    }

    borrar_credenciales(){
        this.key = "";
        this.token = "";
        this.username = "";
        return 0;
    }

    getMiembroActual(){
        let promesa = axios.get(`https://api.trello.com/1/members/${this.username}?key=${this.key}&token=${this.token}`);
        return promesa;
    }

    get_tableros(){
        let promesa = axios.get(`https://api.trello.com/1/members/${this.username}/boards?lists=open&key=${this.key}&token=${this.token}`);
        return promesa;

    }

    get_tarjetas(idLista?: string){
        let promesa = axios.get(`https://api.trello.com//1/lists/${idLista}/cards?fields=id,name,idBoard&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    get_tarjeta(idTarjeta?: string){
        let promesa = axios.get(`https://api.trello.com//1/cards/${idTarjeta}?checklists=all&members=true&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    get_acciones_tarjeta(idTarjeta?: string){
        let promesa = axios.get(`https://api.trello.com//1/cards/${idTarjeta}/actions?filter=all&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    get_primera_tarjeta(){
        axios.get('https://api.trello.com/1/cards/57ace25c4af96f21b247cc23?key=bd067047470f985ed6233104be44081e&token=abd9d736d74486d318b78bdde9d3290ead38bc03959e92f78e42d3f851139fee')
        .then(function (response){
            return response.data;
        });
    }

    delete_tarjeta(idTarjeta?: string){
        let promesa = axios.delete(`https://api.trello.com//1/cards/${idTarjeta}?&key=${this.key}&token=${this.token}`);
        window.showInformationMessage('La tarjeta ha sido eliminada!');
        return promesa;
    }
    
    crear_lista(idTablero: string, nombre: string){ 
        let promesa = axios.post(`https://api.trello.com/1/lists?name=${nombre}&idBoard=${idTablero}&key=${this.key}&token=${this.token}`);
        window.showInformationMessage('La lista ha sido creada!');
        return promesa;
    }

    modificar_lista(idLista: string, nombre: string){ 
        let promesa = axios.put(`https://api.trello.com/1/lists/${idLista}?name=${nombre}&key=${this.key}&token=${this.token}`);
        window.showInformationMessage('La lista ha sido modificada!');
        return promesa;
    }

    delete_lista(idLista?: string){ 
        let promesa = axios.put(`https://api.trello.com/1/lists/${idLista}/closed?value=true&key=${this.key}&token=${this.token}`);
        window.showInformationMessage('La lista ha sido eliminada!');
        return promesa;
    }
    
    actualizarTarjeta(tarjeta: object) {
        let idTarjeta = tarjeta.id;
        delete tarjeta.id;
        let promesa = axios.put(`https://api.trello.com//1/cards/${idTarjeta}?&key=${this.key}&token=${this.token}`, tarjeta);
        window.showInformationMessage('La tarjeta ha sido actualizada!');
        return promesa;
    }

    actualizarTarjetaId(tarjetaId: string, datos:object) {
        let promesa = axios.put(`https://api.trello.com//1/cards/${tarjetaId}?&key=${this.key}&token=${this.token}`, datos);
        window.showInformationMessage('La tarjeta ha sido actualizada!');
        return promesa;
    }

    agregarComentarioTarjeta(idTarjeta: string, comentario:string){
        let texto = encodeURI(comentario);
        let promesa = axios.post(`https://api.trello.com//1/cards/${idTarjeta}/actions/comments?text=${texto}&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    crearTarjeta(nombre: string, desc: string, idLista: string){
        let promesa = axios.post(`https://api.trello.com/1/cards?name=${nombre}&desc=${desc}&idList=${idLista}&key=${this.key}&token=${this.token}`);
        window.showInformationMessage('La tarjeta ha sido creada!');
        return promesa;
    }

    agregarChecklistItem(checklistId: string, nombre: string){
        let nombreUri = encodeURI(nombre);
        let promesa = axios.post(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${nombreUri}&pos=bottom&checked=false&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    atualizarChecklistItem(idTarjeta: string, nombre: string, idChecklistItem: string, estado: string){
        let nombreUri = encodeURI(nombre);
        let promesa = axios.put(`https://api.trello.com/1/cards/${idTarjeta}/checkItem/${idChecklistItem}?name=${nombreUri}&state=${estado}&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    eliminarChecklistItem(idTarjeta: string, idChecklistItem: string){
        let promesa = axios.delete(`https://api.trello.com/1/cards/${idTarjeta}/checkItem/${idChecklistItem}?key=${this.key}&token=${this.token}`);
        return promesa;
    }

    get_listas(idBoard: string){
        let promesa = axios.get(`https://api.trello.com/1/boards/${idBoard}/lists?cards=none&card_fields=all&filter=open&fields=name&key=${this.key}&token=${this.token}`);
        return promesa;
    }

    cambiarLista(idTarjeta: string, idLista: string){
        let promesa = axios.put(`https://api.trello.com/1/cards/${idTarjeta}?idList=${idLista}&key=${this.key}&token=${this.token}`);
        return promesa;
    }

}