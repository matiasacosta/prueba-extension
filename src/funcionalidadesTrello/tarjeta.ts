import * as vscode from 'vscode';

export class Tarjeta extends vscode.TreeItem {
    //Tarjeta item
    constructor(
        public readonly id: string,
        public readonly nombre:string,
        public readonly idBoard:string,
        public readonly listaId: string,
        public readonly listaNombre: string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState,
    ){
        super(nombre, collapsibleState);
    }

    get tooltip(): string {
        return this.nombre;
    }

    get description(): string {
        return this.nombre;
    }

    contextValue = 'tarjeta';

}