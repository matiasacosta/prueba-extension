import * as vscode from 'vscode';
import {Lista} from './lista';

export class Tablero extends vscode.TreeItem {
    
    listas: Lista[];

    constructor(
        public readonly id:string,
        public readonly nombre:string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState,
        listas_api: object[]
    ){
        super(nombre, collapsibleState);
        let listaAux: Lista[] = [];
        for (let lista of listas_api){
            let l = new Lista(lista.id, lista.name, vscode.TreeItemCollapsibleState.None);
            listaAux.push(l);
        }
        this.listas = listaAux;
    }

    get tooltip(): string {
        return this.nombre;
    }

    get description(): string {
        return this.nombre;
    }

    contextValue = 'tablero';

}