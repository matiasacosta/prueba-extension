// El módulo 'vscode' contiene la API de extensiones de VS Code.
import * as vscode from 'vscode';
import { Trello } from './funcionalidadesTrello/trello';
//import fs = require('fs');


// Este metodo es llamado cuando la extensión se activa.
export function activate(context: vscode.ExtensionContext) {

	//let ruta = context.globalStoragePath + 'credenciales.txt';
	//fs.truncate(ruta, 0, function(){console.log('done');}); //borra el contenido del archivo
	console.log(context.globalStoragePath);
	// Creamos una nueva instancia del objecto trello que es capaz de:
	// - Auntenticarse.
	// - Comunicarse con la API de Trello a través del cliente.
	// - Actualizar los paneles de la extensión.
	let trello = new Trello(context);
	//trello.guardar_credenciales(); 
	//trello.get_credenciales();
	trello.loguearse();
	let comandos = trello.get_comandos(context);
	context.subscriptions.concat(comandos);
	// Esta ejecución de comando es importante para
	// poder ver las vistas cuando se activa la extensión
	vscode.commands.executeCommand('setContext', 'workspaceIsOpen', true);
	vscode.workspace.onDidSaveTextDocument((documento)=>{
		trello.asociarArchivo(documento);
	});
}

// Este metodo es llamado cuando la extensión es desactivada.
export function deactivate() {}
