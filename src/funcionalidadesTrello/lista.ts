import * as vscode from 'vscode';

export class Lista extends vscode.TreeItem {
    
    constructor(
        public readonly id:string,
        public readonly nombre:string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState,
    ){
        super(nombre, collapsibleState);
    }

    get tooltip(): string {
        return this.nombre;
    }

    get description(): string {
        return this.nombre;
    }

    contextValue = 'lista';

}