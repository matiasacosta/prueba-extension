import * as vscode from 'vscode';
import { TableroProvider } from './tableroProvider';
import { TarjetaProvider } from './tarjetaProvider';
import fs = require('fs');
import os = require('os');
import { Cliente } from './cliente';
import { Tarjeta } from './tarjeta';
import { Lista } from './lista';
import * as path from 'path';
import { Tablero } from './tablero';
import { Miembro } from './miembro';
import { Entry, FileSystemProvider} from '../fileExplorer';


async function pedirCredenciales(ruta: string) {
    
    let key, token, username: string | undefined;

    key = await vscode.window.showInputBox({ placeHolder: 'Ingrese su API KEY', prompt: 'AUTENTICACION', ignoreFocusOut: true });
    fs.writeFileSync(ruta, key);
    fs.writeFileSync(ruta, '\n', { flag: 'a+' });

    token = await vscode.window.showInputBox({ placeHolder: 'Ingrese su TOKEN', prompt: 'AUTENTICACION', ignoreFocusOut: true });
    fs.writeFileSync(ruta, token, { flag: 'a+' });
    fs.writeFileSync(ruta, '\n', { flag: 'a+' });

    username = await vscode.window.showInputBox({ placeHolder: 'Ingrese su NOMBRE DE USUARIO', prompt: 'AUTENTICACION', ignoreFocusOut: true });
    fs.writeFileSync(ruta, username, { flag: 'a+' });
    fs.writeFileSync(ruta, '\n', { flag: 'a+' });

    let credenciales = fs.readFileSync(ruta, 'utf8');
    console.log(credenciales);

    return credenciales;    
}


export class Trello {

    cliente: Cliente;
    ruta: string;
    rutaTarjeta: string;
    tableroProvider: TableroProvider;
    viewTablero: vscode.TreeView<Tablero | Lista>;
    tarjetaProvider: TarjetaProvider;
    viewTarjeta: vscode.TreeView<Tarjeta>;
    archivoProvider: FileSystemProvider;
    viewArchivo: vscode.TreeView<Entry>;
    tarjeta: Tarjeta | undefined;
    tengoCredenciales: boolean = false;
    tarjetaAsociadaStatus: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
    archivosAsociados: string[] = [];
    usuario?: Miembro;


    constructor(context: vscode.ExtensionContext){
        let datos: object[] = [];

        this.cliente = new Cliente();
        this.ruta = context.globalStoragePath + 'credenciales.txt';
        this.rutaTarjeta = context.globalStoragePath + 'tarjeta.txt';
        this.tableroProvider = new TableroProvider();
        this.viewTablero = vscode.window.createTreeView('viewTablero', {
            treeDataProvider: this.tableroProvider
        });
        this.tarjetaProvider = new TarjetaProvider();
        this.viewTarjeta = vscode.window.createTreeView('viewTarjeta', {
            treeDataProvider: this.tarjetaProvider,
            showCollapseAll: false
        });
        this.archivoProvider = new FileSystemProvider();
        this.viewArchivo = vscode.window.createTreeView('viewArchivo', {
            treeDataProvider: this.archivoProvider,
            showCollapseAll: false
        });
    }

    mostrarTarjetaAsociada(){
        let informacion = fs.readFileSync(this.rutaTarjeta, 'utf8');
        let arregloInformacion = informacion.split("\n");
        if(arregloInformacion.length > 1){
            this.cliente.get_tarjeta(arregloInformacion[0]).then((respuesta) => {
                this.tarjeta = new Tarjeta(respuesta.data.id, respuesta.data.name, respuesta.data.idBoard, respuesta.data.idList, "", vscode.TreeItemCollapsibleState.None);
                this.crearStatusItem(this.tarjeta);
            });
            for(let i = 2; i < arregloInformacion.length -1; i++){
                this.archivosAsociados.push(arregloInformacion[i].replace("/n",""))
            }
            this.archivoProvider = new FileSystemProvider(this.archivosAsociados);
            this.viewArchivo = vscode.window.createTreeView('viewArchivo', {
                treeDataProvider: this.archivoProvider,
                showCollapseAll: false
            });
        } else {
            this.tarjeta = undefined;
        }
    }

    get_tableros_panel_logout(){
        let data: Tablero[];

        let tableros = vscode.window.createTreeView('viewTablero', {
            treeDataProvider: new TableroProvider(data),
        });
        this.tarjetaProvider.setElementosNull();
        this.tarjetaProvider.refresh();
    }

    get_tableros_panel(){
        let data = this.cliente.get_tableros();
        data.then(response => { //cuando se resuelve la promesa creamos el TreeDataProvider
            this.tableroProvider.setTableros(response.data);
            this.tableroProvider.refresh();
            this.viewTablero.onDidChangeSelection(seleccion => {
                if (seleccion.selection[0].contextValue === 'lista'){
                    let data_tarjetas = this.cliente.get_tarjetas(seleccion.selection[0].id);
                    data_tarjetas.then(response =>{
                        this.tarjetaProvider.setElementos(response.data, seleccion.selection[0].id, seleccion.selection[0].nombre);
                        this.tarjetaProvider.refresh();
                    });
                }  
            });
        });
    }

    get_comandos(context: vscode.ExtensionContext){
        let panel: vscode.WebviewPanel | undefined = undefined;
        
        let abrirTarjetaStatus = vscode.commands.registerCommand('trello.abrirTarjetaDesdeStatus',() => {
            vscode.commands.executeCommand('trello.editarTarjeta',this.tarjeta);
        });

        let comandoLogIn = vscode.commands.registerCommand('trello.LogIn',(tablero: Tablero) => {
            this.get_credenciales();
        });
        
        let comandoLogOut = vscode.commands.registerCommand('trello.LogOut',(tablero: Tablero) => {
            vscode.window.showInformationMessage(`Esta seguro que desea desloguearse?`, 
                                                 ...['Si', 'No'])
                .then(selection => {
                        if (selection === "Si"){
                            this.desloguearse();
                        }
                });
        });

        let comandoCrearLista = vscode.commands.registerCommand('trello.crearLista',(tablero: Tablero) => {
            vscode.window.showInputBox({ placeHolder: 'Ingrese nombre de la lista', prompt: 'Crear Lista' }).then(nombre => {
                if(nombre){
                    let seCreo = this.cliente.crear_lista(tablero.id, nombre);
                    seCreo.then(() => {
                        this.get_tableros_panel();
                    });
                }
            });
        });

        let comandoModificarLista = vscode.commands.registerCommand('trello.modificarLista',(lista: Lista) => {
            vscode.window.showInputBox({ placeHolder: 'Ingrese el nuevo nombre de la lista', prompt: 'Modificar Lista' }).then(nombre => {
                if(nombre){
                    let seCreo = this.cliente.modificar_lista(lista.id, nombre);
                    seCreo.then(() => {
                        this.get_tableros_panel();
                    });
                }
            });
        });
        
        let comandoBorrarLista = vscode.commands.registerCommand('trello.borrarLista',(lista: Lista) => {
            vscode.window.showInformationMessage(`Esta seguro que desea eliminar la lista ${lista.nombre}?`, 
                                                 ...['Si', 'No'])
                .then(selection => {
                        if (selection === "Si"){
                            let seBorro = this.cliente.delete_lista(lista.id);
                            seBorro.then(() => {
                                this.get_tableros_panel();
                                this.tarjetaProvider.setElementosNull();
                                this.tarjetaProvider.refresh();
                            });
                        }
                });
        });

        let comandoCrear = vscode.commands.registerCommand('trello.crearTarjeta',(lista: Lista) => {
            
            if(panel){
                panel.dispose();
            }
            // Creamos y mostramos una webview
            panel = vscode.window.createWebviewPanel(
                '1', // Identifica el panel, es usado internamente
                'nueva tarjeta', // Titulo del panel, aparece en la pestaña
                vscode.ViewColumn.Two, // Elegis la columna donde queres que aparesca, yo puse la 2 para que no te tape el codigo
                {
                    // Webview options.
                    enableScripts: true, //para poder ejecutar JS, por defecto esta desactivado!
                    localResourceRoots: [vscode.Uri.file(path.join(context.extensionPath, 'media'))] //Son las rutas desde donde el panel puede cargar archivos
                } 
            );
            
            panel.onDidDispose(
                () => {
                    panel = undefined;
                },
                null,
                context.subscriptions
            );
            
            // Maneja los mensajes desde el webview
            panel.webview.onDidReceiveMessage(
                mensaje => {
                    let nuevaTarjeta = this.cliente.crearTarjeta(mensaje.name, mensaje.desc, mensaje.listaId);
                    nuevaTarjeta.then(() => { //Actualizo el treeProvider con el refresh
                        let t = new Tarjeta('999', 'tarjeta', '1', mensaje.listaId, 'lista', vscode.TreeItemCollapsibleState.None);
                        let data_tarjetas = this.cliente.get_tarjetas(mensaje.listaId);
                        data_tarjetas.then(response =>{
                            this.tarjetaProvider.setElementos(response.data, mensaje.listaId, lista.nombre);
                            this.tarjetaProvider.refresh();
                        });
                        panel.dispose();
                    });
                },
                undefined,
                context.subscriptions
            );

            const jQuery = vscode.Uri.file(
                path.join(context.extensionPath, 'media', 'jquery', 'jquery-3.1.1.min.js')
            );
            const urljQuery = jQuery.with({ scheme: 'vscode-resource' });
            
            const semanticCss = vscode.Uri.file(
                path.join(context.extensionPath, 'media', 'semantic','dist', 'semantic.min.css')
            );
            const urlSemanticCss = semanticCss.with({ scheme: 'vscode-resource' });

            const semanticJs = vscode.Uri.file(
                path.join(context.extensionPath, 'media', 'semantic','dist', 'semantic.min.js')
            );
            const urlSemanticJs = semanticJs.with({ scheme: 'vscode-resource' });
            
            panel.webview.html = this.getWebviewContent_crearTarjeta(
                urljQuery, urlSemanticCss, urlSemanticJs, lista
            );
            
        });

        let comandoEditar = vscode.commands.registerCommand('trello.editarTarjeta',(tarjeta: Tarjeta) => {

            if (panel && tarjeta.nombre === panel.title){
                panel.reveal(vscode.ViewColumn.Two);
            }else{
                if(panel){
                    panel.dispose();
                }
                // Creamos y mostramos una webview
                panel = vscode.window.createWebviewPanel(
                    tarjeta.id, // Identifica el panel, es usado internamente
                    tarjeta.nombre, // Titulo del panel, aparece en la pestaña
                    vscode.ViewColumn.Two, // Elegis la columna donde queres que aparesca, yo puse la 2 para que no te tape el codigo
                    {
                        // Webview options.
                        enableScripts: true, //para poder ejecutar JS, por defecto esta desactivado!
                        localResourceRoots: [vscode.Uri.file(path.join(context.extensionPath, 'media'))], //Son las rutas desde donde el panel puede cargar archivos
                        retainContextWhenHidden: true
                    } 
                );
                
                panel.onDidDispose(
                    () => {
                        panel = undefined;
                    },
                    null,
                    context.subscriptions
                );
                
                const jQuery = vscode.Uri.file(
                    path.join(context.extensionPath, 'media', 'jquery', 'jquery-3.1.1.min.js')
                );
                const urljQuery = jQuery.with({ scheme: 'vscode-resource' });
                
                const semanticCss = vscode.Uri.file(
                    path.join(context.extensionPath, 'media', 'semantic','dist', 'semantic.min.css')
                );
                const urlSemanticCss = semanticCss.with({ scheme: 'vscode-resource' });
    
                const semanticJs = vscode.Uri.file(
                    path.join(context.extensionPath, 'media', 'semantic','dist', 'semantic.min.js')
                );
                const urlSemanticJs = semanticJs.with({ scheme: 'vscode-resource' });

                const tarjetaCss = vscode.Uri.file(
                    path.join(context.extensionPath, 'media', 'styles', 'tarjeta.css')
                );
                const urlTarjetaCss = tarjetaCss.with({ scheme: 'vscode-resource' });

                let listas = this.cliente.get_listas(tarjeta.idBoard);
                
                listas.then(responseLista => {
                    this.cliente.get_tarjeta(tarjeta.id).then(responseTarjeta => {
                        this.cliente.get_acciones_tarjeta(tarjeta.id).then(responseAcciones => {
                            let estaAsociada = (this.tarjeta != undefined && this.tarjeta.id == tarjeta.id);
                            panel.webview.html = this.getWebviewContent(
                                urljQuery, urlSemanticCss, urlSemanticJs, urlTarjetaCss,
                                responseTarjeta.data, responseAcciones.data, tarjeta.listaNombre, this.usuario, 
                                estaAsociada, responseLista.data
                            );
                        });
                    });
                });


                // Manejamos el mensaje del WebView, puede llegar seis tipos de mensajes:
                // - Un mensaje para la actualización asincrona de la tarea, que llega cada X cantidad de tiempo.
                // - Un mensaje para avisarme que asocie la tarjeta actual a la extensión.
                // - Un mensaje donde le mandamos un comentario para agregar
                // - Un mensaje para agregar un checklistItem a un checklist
                // - Un mensaje para eliminar un checklistItem a un checklist
                // - Un mensaje para actualizar un checklistItem a un checklist
                panel.webview.onDidReceiveMessage(
                    mensaje => {
                        switch(mensaje.accion){
                            case 'actualizarTarjeta':{
                                //Agregamos la parte de la extension basada en el archivo
                                if(this.tarjeta && mensaje.tarjeta.id === this.tarjeta.id){
                                    console.log("entre");
                                    mensaje.tarjeta.desc = mensaje.tarjeta.desc + '\n';
                                    mensaje.tarjeta.desc = mensaje.tarjeta.desc + "Archivos Asociados:" + '\n\n';
                                    for(let archivo of this.archivosAsociados){
                                        mensaje.tarjeta.desc = mensaje.tarjeta.desc + '- ' + archivo + '\n';
                                    }
                                }
                                this.cliente.actualizarTarjeta(mensaje.tarjeta).then(response => {
                                    return "termine";
                                }).then(() => {
                                    let idLista = mensaje.tarjeta.listId;
                                    this.cliente.get_tarjetas(idLista).then(response => {
                                        this.tarjetaProvider.setElementos(response.data, idLista, mensaje.tarjeta.listName);
                                        this.tarjetaProvider.refresh();
                                    });
                                });
                                break;
                            }
                            case 'asociarTarjeta':{
                                this.asociarTarjeta(mensaje.tarjeta.id);
                                break;
                            }
                            case 'desasociarTarjeta':{
                                this.desasociarTarjeta(mensaje.tarjeta.id);
                                break;
                            }
                            case 'sincronizarTarjeta':{
                                let listas = this.cliente.get_listas(mensaje.tarjeta.idBoard);
                
                                listas.then(responseLista => {
                                    this.cliente.get_tarjeta(mensaje.tarjeta.id).then(responseTarjeta => {
                                        this.cliente.get_acciones_tarjeta(tarjeta.id).then(responseAcciones => {
                                            let estaAsociada = (this.tarjeta != undefined && this.tarjeta.id == mensaje.tarjeta.id);
                                            panel.webview.html = this.getWebviewContent(
                                                urljQuery, urlSemanticCss, urlSemanticJs, urlTarjetaCss,
                                                responseTarjeta.data, responseAcciones.data, tarjeta.listaNombre, this.usuario, 
                                                estaAsociada, responseLista.data
                                            );
                                        });
                                    });
                                });
                                break;
                            }
                            case 'agregarComentario':{
                                this.cliente.agregarComentarioTarjeta(mensaje.tarjeta.id, mensaje.comentario);
                                break;
                            }
                            case 'agregarChecklistItem':{
                                this.cliente.agregarChecklistItem(mensaje.checklistId, mensaje.nombre).then(response => {
                                    let info = response.data;
                                    panel.webview.postMessage({
                                        comando: 'agregarChecklistItem',
                                        checklistId: info.idChecklist,
                                        elementoId: info.id,
                                        nombre: info.name
                                    });
                                });
                                break;
                            }
                            case 'eliminarChecklistItem':{
                                this.cliente.eliminarChecklistItem(mensaje.idTarjeta, mensaje.idChecklistItem);
                                break;
                            }
                            case 'actualizarChecklistItem':{
                                this.cliente.atualizarChecklistItem(mensaje.idTarjeta, mensaje.name, mensaje.idChecklistItem, mensaje.state);
                                break;
                            }
                            case 'cambiarLista':{
                                this.cliente.cambiarLista(mensaje.idTarjeta, mensaje.listaId).then(response => {
                                    return "termine";
                                }).then(() => {
                                    this.cliente.get_tarjetas(mensaje.listaIdViejo).then(response => {
                                        this.tarjetaProvider.setElementos(response.data, mensaje.listaIdViejo, mensaje.listName);
                                        this.tarjetaProvider.refresh();
                                    });
                                });
                                break;
                            }
                        }
                    },
                    undefined,
                    context.subscriptions
                );
            }
        });
        let comandoBorrar = vscode.commands.registerCommand('trello.borrarTarjeta',(tarjeta: Tarjeta) => {
            vscode.window.showInformationMessage(`Esta seguro que desea eliminar la tarjeta ${tarjeta.label}?`, 
                                                 ...['Si', 'No'])
                .then(selection => {
                        if (selection === "Si"){
                            let seBorro = this.cliente.delete_tarjeta(tarjeta.id);
                            seBorro.then(() => {
                                let data_tarjetas = this.cliente.get_tarjetas(tarjeta.listaId);
                                data_tarjetas.then(response =>{
                                    this.tarjetaProvider.setElementos(response.data, tarjeta.listaId, tarjeta.listaNombre);
                                    this.tarjetaProvider.refresh();
                                });
                            });
                        }
                });
        });
        let comandoAsociar = vscode.commands.registerCommand('trello.asociarTarjeta',(tarjeta: Tarjeta) => {
            this.asociarTarjeta(tarjeta.id);
            vscode.window.showInformationMessage(`Se ha asociado la tarjeta: ${tarjeta.nombre}`);
        });
        let abrirArchivo = vscode.commands.registerCommand('trello.abrirArchivo', (resource) => this.openResource(resource));
        let desasociarArchivo = vscode.commands.registerCommand('trello.desasociarArchivo', (archivo)=>{
            this.desasociarArchivo(archivo);
        });
        let comandos: vscode.Disposable[] = [
            abrirTarjetaStatus,
            comandoCrear, comandoEditar, comandoBorrar, comandoAsociar, abrirArchivo,
            comandoLogIn, comandoLogOut, comandoCrearLista, comandoModificarLista, comandoBorrarLista,
            desasociarArchivo
        ];
        return comandos;
    }

    openResource(resource: vscode.Uri): void {
		vscode.window.showTextDocument(resource);
	}

    desasociarArchivo(archivo: Entry){
        let carpetaRuta = vscode.workspace.workspaceFolders ? vscode.workspace.workspaceFolders[0].uri.path : undefined ;
        //Sacamos el archivo del arreglo, despues del archivo del globalStorage finalmente de la tarjeta.
        if(carpetaRuta && archivo.uri.path.includes(carpetaRuta)){
            let nombreArchivo = archivo.uri.path.slice(carpetaRuta.length + 1);
            this.archivosAsociados.splice(this.archivosAsociados.indexOf(nombreArchivo), 1);

            let informacion = fs.readFileSync(this.rutaTarjeta, 'utf8');
            let arregloInformacion = informacion.split("\n");
            if(arregloInformacion.length > 2){
                arregloInformacion.splice(arregloInformacion.indexOf(nombreArchivo), 1);
                fs.writeFileSync(this.rutaTarjeta, arregloInformacion.join("\n"));
            }

            this.cliente.get_tarjeta(arregloInformacion[0]).then((respuesta) => {
                let arregloDesc = respuesta.data.desc.split("\n");
                arregloDesc.splice(arregloDesc.indexOf("- "+nombreArchivo), 1);
                this.cliente.actualizarTarjetaId(arregloInformacion[0],{desc:arregloDesc.join("\n")}).then(()=>{
                    console.log("Desasocie el archivo correctamente");
                });
            });
            this.actualizarVistaArchivos();
        }
    }

    guardar_credenciales(){
        pedirCredenciales(this.ruta).then(credenciales => {
            this.cliente.autenticarse(credenciales);
            this.cliente.getMiembroActual().then(response => {
                this.usuario = new Miembro(response.data);
            });
            vscode.window.showInformationMessage("Sus credenciales han sido guardadas");
            this.get_tableros_panel();
        });
    }

    get_credenciales(){
        //se crea el archivo
        let trello = this;
        fs.openSync(trello.ruta, 'w'); 
        trello.guardar_credenciales();
    }

    loguearse(){
        //se fija si ya posee las claves
        let credenciales: any;
        fs.exists(this.ruta, (exists) => {
            if(exists){
                credenciales = fs.readFileSync(this.ruta, 'utf8');
                if(credenciales.length > 1){
                    this.cliente.autenticarse(credenciales);
                    this.cliente.getMiembroActual().then(response => {
                        this.usuario = new Miembro(response.data);
                    });
                    this.get_tableros_panel();
                    this.tengoCredenciales = true;
                    this.mostrarTarjetaAsociada();
                }
            }    
        });    
    }
    
    desloguearse(){
        fs.unlink(this.ruta, (err) => {
            if (err) {
              console.error(err);
              return;
            }
            vscode.window.showInformationMessage("Sus credenciales han sido eliminadas");
            this.cliente.borrar_credenciales();
            this.get_tableros_panel_logout();
            this.tengoCredenciales = false;
        });   
    }
    
    // Metodo que se llama para poder asociar una nueva tarjeta.
    // Por un lado debe escribir el archivo de tarjeta para persistir la tarjeta asociada.
    // Por otro lado debe cambiar el Id de la tarjeta asociada.
    asociarTarjeta(tarjetaId: string){
        let descripcionTarjeta, archivos: string;
        if (this.tengoCredenciales){
            this.cliente.get_tarjeta(tarjetaId).then((respuesta) => {
                this.tarjeta = new Tarjeta(respuesta.data.id, respuesta.data.name, respuesta.data.idBoard, respuesta.data.idList, "", vscode.TreeItemCollapsibleState.None);
                this.crearStatusItem(this.tarjeta);
                descripcionTarjeta = respuesta.data.desc;
                // Realizo un analisis de la descripción, si tengo archivos asociados lo pongo
                let posicion = descripcionTarjeta.indexOf("Archivos Asociados:");
                if (posicion >= 0){
                    archivos = descripcionTarjeta.slice(posicion);
                    let arregloArchivos = archivos.split("\n");
                    archivos = "Archivos Asociados:\n";
                    for(let i = 1; i < arregloArchivos.length; i++){
                        //Esto se hace para sacarle el "- " previo que tiene cada archivo asociado a la tarjeta
                        if(arregloArchivos[i] !== ""){
                            archivos = archivos + arregloArchivos[i].slice(2)+"\n";
                            //Tambien agrego los archivos asociados al global storage aunque no pertenezcan a la carpeta actual
                            if (! this.archivosAsociados.find((cadena) => cadena === arregloArchivos[i].slice(2))){
                                this.archivosAsociados.push(arregloArchivos[i].slice(2));
                            }
                        }
                    }
                    this.actualizarVistaArchivos();
                }else{
                    archivos = 'Arcivos Asociados:\n';
                }
                // Guardo el Id en el archivo y los archivos en su path relativo
                console.log('Guardando en el archivo...');
                fs.writeFileSync(this.rutaTarjeta, tarjetaId);
                fs.writeFileSync(this.rutaTarjeta, '\n', { flag: 'a+' });
                fs.writeFileSync(this.rutaTarjeta, archivos, { flag: 'a+' });
            });
        }
    }

    // Metodo que se llama para poder desasociar una tarjeta
    // Por un lado debe borrar el archivo de la tarjeta asociada.
    // Por otro lado debe cambiar el Id de la tarjeta asociada a null o undefinded
    desasociarTarjeta(tarjetaId: string){
        if (this.tengoCredenciales){
            this.borrarStatusItem();
            // Borro el archivo de la tarjeta
            fs.unlinkSync(this.rutaTarjeta);
            this.tarjeta = undefined;
            this.archivosAsociados = [];
            this.actualizarVistaArchivos();
        }
    }

    getWebviewContent_crearTarjeta(jQuery: vscode.Uri, semanticCss: vscode.Uri, semanticJs: vscode.Uri, lista: Lista) {
		return `
		<!DOCTYPE html>
		<html>
			<head>
                <!--Import semantic.css-->
                <link rel="stylesheet" href=${semanticCss}>
				<!--Let browser know website is optimized for mobile-->
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body>
				<div class="ui inverted segment">
                <div class="ui grid">
                    <div class="ten wide column">
                        <h2 id="tituloTarjeta" class="ui header inverted">Titulo</h2>
                        <input type="text" id="inputTituloTarjeta">
                    </div>
                </div>
                    <button class="mini ui right floated inverted blue basic button" onclick="guardar()">Guardar Tarjeta</button>
                    <h3 id="listaTarjeta" class="ui header">Lista: ${lista.nombre}</h3>  
                    <div class="ui form">
                        <div class="field">
                        <label style="color: white">Descripción</label>
                        <textarea id="descripcion" rows="5"></textarea>
                        </div>
                    </div>
                    <br>
                    <!--Checklists-->
                    <!--Le vamos a permitir crear checklist?? -->
                    <!-- Comentarios -->
                    <div class="ui form">
                        <div class="field">
                        <label style="color: white">Comentarios</label>
                        <textarea id="comentarioTarjeta" rows="5"></textarea>
                                </div>
                        <button class="mini ui right floated inverted green basic button">Guardar</button>
                    </div>
                    <!-- Actividades -->
                    <label style="color: white">Actividades</label>
                    <div id="listaActividades" class="ui list">
                    </div>
    		    </div>
                <!-- Compiled and minified JavaScript -->
                <script src=${jQuery}></script>
                <script src=${semanticJs}></script>
                <script>
                    const vscode = acquireVsCodeApi();

                    function guardar() {
                        vscode.postMessage({
                            'name': $('#tituloTarjeta').text(),
                            'desc': $('#descripcion').val(),
                            'listaId': '${lista.id}'
                        })  
                    }

                    // Para agregar el input al titulo
                    $(function () {
                        $('#inputTituloTarjeta').hide();
                        $('#tituloTarjeta').click(function () {
                            $(this).hide();
                            $('#inputTituloTarjeta').val($(this).html().trim());
                            $('#inputTituloTarjeta').show();
                        });
                        $('#inputTituloTarjeta').keypress(function(event){
                            let keycode = (event.keyCode ? event.keyCode : event.which);
                            if(keycode == '13'){
                                $('#inputTituloTarjeta').hide();
                                $('#tituloTarjeta').html($('#inputTituloTarjeta').val());
                                $('#tituloTarjeta').show();
                                vscode.postMessage(serializarTarjeta('actualizarTarjeta'));
                            }
                        });
                    });

                </script>
			</body>
		</html>
		`;
	}

    getWebviewContent(jQuery: vscode.Uri,semanticCss: vscode.Uri,
        semanticJs: vscode.Uri, tarjetaCss: vscode.Uri,
        tarjetaTrello: any, tarjetaAcciones: any, nombreLista: string, 
        usuario: Miembro | undefined, estaAsociada: boolean,
        listas: any) {
		return `
		<!DOCTYPE html>
		<html>
			<head>
				<!--Import semantic.css-->
                <link rel="stylesheet" href=${semanticCss}>
                <!--Import estilos propios de la tarjeta-->
                <link rel="stylesheet" href=${tarjetaCss}>
				<!--Let browser know website is optimized for mobile-->
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body>
				<div class="ui inverted segment">
                    <div class="ui grid">
                        <div class="ten wide column">
                            <h2 id="tituloTarjeta" class="ui header inverted">${tarjetaTrello.name}</h2>
                            <input type="text" id="inputTituloTarjeta">
                        </div>
                        <div class="six wide column">
                            <i class="inverted large blue sync icon" onclick="sincronizarTarjeta()"></i>
                            ${this.getAccion(estaAsociada)}
                        </div>
                    </div>
                    <p id="listas">en la lista:&nbsp;
                        
                                ${this.getListas(listas)}
                        
                    </p>
                    <div class="ui grid">
                        <br>
                        ${this.getMiembros(tarjetaTrello.members)}
                        ${this.getEtiquetas(tarjetaTrello.labels)}
                    </div>
                    <div class="ui form" style="padding-top: 25px;">
                        <div class="field">
                        <label style="color: white">Descripción</label>
                        <textarea id="descripcion" rows="5">${tarjetaTrello.desc}</textarea>
                        </div>
                    </div>
                    <br>
                    <!--Checklists-->
                    ${this.getChecklists(tarjetaTrello.checklists)}
                    <!-- Comentarios -->
                    <div class="ui form">
                        <div class="field">
                            <label style="color: white">Comentarios</label>
                            <textarea id="comentarioTarjeta" rows="5"></textarea>
                        </div>
                        <button class="mini ui right floated inverted green basic button" onclick="agregarComentario()">Guardar</button>
                    </div>
                    <!-- Actividades -->
                    <label style="color: white">Actividades</label>
                    <div id="listaActividades" class="ui list">
                        ${this.getActividades(tarjetaAcciones)}
                    </div>
    		    </div>
                <!-- Compiled and minified JavaScript -->
                <script src=${jQuery}></script>
                <script src=${semanticJs}></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script>
                    const vscode = acquireVsCodeApi();
                    // Para cambiar la lista de la tarjeta
                    $(function() {
                        document.getElementById("select").value = '${tarjetaTrello.idList}';
                    });
                    function cambiarLista() {
                        listaId = document.getElementById("select").value;
                        vscode.postMessage(
                            {'accion': 'cambiarLista', 'idTarjeta': '${tarjetaTrello.id}', 'listaId': listaId, 
                                       'listaIdViejo': '${tarjetaTrello.idList}', 'listName': '${tarjetaTrello.listName}'}
                        );
                    }
                    // Para agregar el input al titulo
                    $(function () {
                        $('#inputTituloTarjeta').hide();
                        $('#tituloTarjeta').click(function () {
                            $(this).hide();
                            $('#inputTituloTarjeta').val($(this).html().trim());
                            $('#inputTituloTarjeta').show();
                        });
                        $('#inputTituloTarjeta').keypress(function(event){
                            let keycode = (event.keyCode ? event.keyCode : event.which);
                            if(keycode == '13'){
                                $('#inputTituloTarjeta').hide();
                                $('#tituloTarjeta').html($('#inputTituloTarjeta').val());
                                $('#tituloTarjeta').show();
                                vscode.postMessage(serializarTarjeta('actualizarTarjeta'));
                            }
                        });
                    });
                    // detecto que se cambio la descripcion
                    $('#descripcion').change(function(){
                        vscode.postMessage(serializarTarjeta('actualizarTarjeta'));
                    });
                    // para los inputos en los checklistItem
                    $(function () {
                        $('.checklistItemInp').each(function() {
                            $( this ).hide();
                        });
                        $('.checkbox').children('label').click(function () {
                            $(this).parent().hide();
                            $(this).parent().next().children('input').val($(this).html());
                            $(this).parent().next().show();
                        });
                        $('.checklistItemInp input').keypress(function(event){
                            let keycode = (event.keyCode ? event.keyCode : event.which);
                            if(keycode == '13'){
                                if($(this).val() != ''){ //Cambio el input y aviso a trello que cambie
                                    $(this).parent().hide();
                                    $(this).parent().prev().children('label').html($(this).val());
                                    $(this).parent().prev().show();
                                    vscode.postMessage({
                                        'accion': 'actualizarChecklistItem',
                                        'idTarjeta': '${tarjetaTrello.id}',
                                        'name':$(this).parent().prev().children('label').html(),
                                        'idChecklistItem': $(this).parent().parent().prop("id"),
                                        'state': ($(this).parent().prev().children('input').prop("checked") == true ? 'complete' : 'incomplete')
                                    });
                                }else{ //Borro el item y le aviso a trello que lo borre
                                    vscode.postMessage({
                                        'accion': 'eliminarChecklistItem',
                                        'idTarjeta': '${tarjetaTrello.id}',
                                        'idChecklistItem': $(this).parent().parent().prop("id")
                                    });
                                    $(this).parent().parent().remove();
                                }
                            }
                        });
                    });
                    function sincronizarTarjeta(){
                        vscode.postMessage(
                            {
                                'accion':'sincronizarTarjeta',
                                'tarjeta': {
                                    'id': '${tarjetaTrello.id}',
                                    'idBoard': '${tarjetaTrello.idBoard}'
                                }
                            }
                        );
                    }
                    function agregarChecklistItem(checklistId){
                        vscode.postMessage(
                            {
                                'accion':'agregarChecklistItem',
                                'checklistId': checklistId,
                                'nombre': 'Nuevo'
                            }
                        );
                    }

                    function agregarComentario(){
                        let comentario = $('#comentarioTarjeta');
                        if(comentario.val() != ''){
                            let div = document.createElement('div');
                            div.className = 'item';
                            div.innerHTML =
                            '<img class="ui avatar image" src="${usuario.avatarUrl + '/50.png'}">\
                                <div class="content">\
                                <div class="header" style="color:white"><a>${usuario.fullName}</a> ${new Date().toLocaleString()}</div>\
                                <div class="description" style="color:white">\
                                    <div class="ui compact segment">\
                                        <p>' + comentario.val() + '</p>\
                                    </div>\
                                </div>\
                                </div>';

                            $( "#listaActividades" ).prepend(div);
                            vscode.postMessage(
                                {
                                    'accion': 'agregarComentario', 
                                    'tarjeta': {
                                        'id': '${tarjetaTrello.id}'
                                    },
                                    'comentario':comentario.val()
                                }
                            );
                            comentario.val('');
                        }
                    }
                    function serializarTarjeta(accion){
                        return {
                            'accion': accion,
                            'tarjeta': {
                                'id': '${tarjetaTrello.id}',
                                'name': $('#tituloTarjeta').text(),
                                'desc': $('#descripcion').val(),
                                'listName': '${nombreLista}',
                                'listId': '${tarjetaTrello.idList}',
                            }
                        }
                    }
                    function asociarTarjeta(){
                        vscode.postMessage(serializarTarjeta('asociarTarjeta'));
                        $('#accionTarjeta').removeClass('green');
                        $('#accionTarjeta').addClass('red');
                        $('#accionTarjeta').attr("onclick","desasociarTarjeta()");
                        $('#accionTarjeta').text('Desasociar Tarjeta');
                    }
                    function desasociarTarjeta(){
                        vscode.postMessage(serializarTarjeta('desasociarTarjeta'));
                        $('#accionTarjeta').removeClass('red');
                        $('#accionTarjeta').addClass('green');
                        $('#accionTarjeta').attr("onclick","asociarTarjeta()");
                        $('#accionTarjeta').text('Asociar Tarjeta');
                    }

                    $(document).ready(function(){
                        $('input[type="checkbox"]').click(function(){
                            let estado = '';
                            if($(this).prop("checked") == true){
                                estado = 'complete';
                            }
                            else if($(this).prop("checked") == false){
                                estado = 'incomplete';
                            }
                            console.log(estado);
                            vscode.postMessage({
                                'accion': 'actualizarChecklistItem',
                                'idTarjeta': '${tarjetaTrello.id}',
                                'name':$(this).parent().children('label').html(),
                                'idChecklistItem': $(this).parent().parent().prop("id"),
                                'state': estado
                            });
                        });
                    });
                    // Manejamos un mensaje que viene desde la extension
                    window.addEventListener('message', evento => {

                        const mensaje = evento.data; // El JSON QUE MANDA LA EXTENSION

                        switch (mensaje.comando) {
                            case 'agregarChecklistItem':
                                let checklistId = mensaje.checklistId;
                                let div = document.createElement('div');
                                div.className = 'field';
                                div.id = mensaje.elementoId;
                                div.innerHTML =
                                '<div class="ui checkbox">\
                                    <input type="checkbox" name=' + mensaje.nombre + '>\
                                    <label style="color: white">' + mensaje.nombre + '</label>\
                                </div>\
                                <div class="ui mini input checklistItemInp">\
                                    <input type="text">\
                                </div>';
                                document.getElementById(checklistId).appendChild(div);
                                //Agregamos los listeners y eso
                                let jdiv = $(div);

                                jdiv.children('div .checklistItemInp').hide();
                                
                                jdiv.children('div .checkbox').children('label').click(function () {
                                    $(this).parent().hide();
                                    $(this).parent().next().children('input').val($(this).html());
                                    $(this).parent().next().show();
                                });
                                jdiv.children('div .checklistItemInp').children('input').keypress(function(event){
                                    let keycode = (event.keyCode ? event.keyCode : event.which);
                                    if(keycode == '13'){
                                        if($(this).val() != ''){ //Cambio el input y aviso a trello que cambie
                                            $(this).parent().hide();
                                            $(this).parent().prev().children('label').html($(this).val());
                                            $(this).parent().prev().show();
                                            vscode.postMessage({
                                                'accion': 'actualizarChecklistItem',
                                                'idTarjeta': '${tarjetaTrello.id}',
                                                'name':$(this).parent().prev().children('label').html(),
                                                'idChecklistItem': $(this).parent().parent().prop("id"),
                                                'state': (($(this).parent().prev().children('input').prop("checked") == true) ? 'complete' : 'incomplete')
                                            });
                                        }else{ //Borro el item y le aviso a trello que lo borre
                                            vscode.postMessage({
                                                'accion': 'eliminarChecklistItem',
                                                'idTarjeta': '${tarjetaTrello.id}',
                                                'idChecklistItem': $(this).parent().parent().prop("id")
                                            });
                                            $(this).parent().parent().remove();
                                        }
                                    }
                                });
                                $('input[type="checkbox"]').click(function(){
                                    let estado = '';
                                    if($(this).prop("checked") == true){
                                        estado = 'complete';
                                    }
                                    else if($(this).prop("checked") == false){
                                        estado = 'incomplete';
                                    }
                                    console.log(estado);
                                    vscode.postMessage({
                                        'accion': 'actualizarChecklistItem',
                                        'idTarjeta': '${tarjetaTrello.id}',
                                        'name':$(this).parent().children('label').html(),
                                        'idChecklistItem': $(this).parent().parent().prop("id"),
                                        'state': estado
                                    });
                                });
                                break;
                        }
                    });
                </script>
			</body>
		</html>
		`;
	}

    getAccion(estaAsociada: boolean){
        if (!estaAsociada){
            return `<button id="accionTarjeta" class="mini ui right floated inverted green button" onclick="asociarTarjeta()">Asociar Tarjeta</button>`;
        }else{
            return `<button id="accionTarjeta" class="mini ui right floated inverted red button" onclick="desasociarTarjeta()">Desasociar Tarjeta</button>`;
        }
    }

    getChecklists(checklists: any){
        let checktlists_html: string = '';
        for(let checklist of checklists){
            checktlists_html += `<div class="ui form">
                <div id="${checklist.id}" class="grouped fields">
                    <label style="color: white">${checklist.name}</label>`;
            for (let elemento of checklist.checkItems){
                checktlists_html += `${this.getElementoChecklist(elemento)}`;
            }
            checktlists_html += `</div>
                <button class="mini ui left floated inverted blue basic button" onclick="agregarChecklistItem('${checklist.id}')">Añada un elemento</button>
            </div>
            <br><br>`;
        }
        return checktlists_html;
    }

    getElementoChecklist(elemento: any){
        let elementoHtml: string = '';
        let completo:string;
        if(elemento.state === "complete"){
            completo = 'checked';
        }else{
            completo = '';
        }
        elementoHtml += `<div class="field" id="${elemento.id}">
            <div class="ui checkbox">
                <input type="checkbox" name="${elemento.name}" ${completo}>
                <label style="color: white" >${elemento.name}</label>
            </div>
            <div class="ui mini input checklistItemInp">
                <input type="text">
            </div>
        </div>`;
        return elementoHtml;
    }

    getActividades(actividades: any){
        let actividadesHtml: string = '';
        for(let actividad of actividades){
            let mensaje:string;
            switch(actividad.type) { 
                case 'addChecklistToCard': { 
                    mensaje = `<b>${actividad.memberCreator.fullName}</b> ha añadido ${actividad.data.checklist.name} a esta tarjeta`;
                    break; 
                } 
                case 'updateCheckItemStateOnCard': {
                    if(actividad.data.checkItem.state === 'complete'){
                        mensaje = `<b>${actividad.memberCreator.fullName}</b> ha completado ${actividad.data.checkItem.name} en esta tarjeta`;
                    }else{
                        mensaje = `<b>${actividad.memberCreator.fullName}</b> ha marcado ${actividad.data.checkItem.name} como incompleto en esta tarjeta`;
                    }
                    break; 
                } 
                case 'removeChecklistFromCard': { 
                    mensaje = `<b>${actividad.memberCreator.fullName}</b> ha quitado ${actividad.data.checklist.name} de esta tarjeta`;
                    break; 
                } 
                case 'addMemberToCard': { 
                    if(actividad.member.id === actividad.memberCreator.id){
                        mensaje = `<b>${actividad.memberCreator.fullName}</b> se ha unido a esta tarjeta`;
                    }else{
                        mensaje = `<b>${actividad.memberCreator.fullName}</b> ha añadido a <b>${actividad.member.fullName}</b> a esta tarjeta`;
                    }
                    break; 
                } 
                case 'commentCard': { 
                    mensaje = `${actividad.data.text}`;
                    break; 
                } 
                case 'updateCard': { 
                    mensaje = `<b>${actividad.memberCreator.fullName}</b> ha actualizado la tarjeta`;
                    break; 
                } 
                default: { 
                    mensaje = `<b>${actividad.memberCreator.fullName}</b> ha actualizado la tarjeta`;
                    break; 
                } 
            }
            if(actividad.type === 'commentCard'){
                actividadesHtml += `<div class="item">
                <img class="ui avatar image" src="${actividad.memberCreator.avatarUrl + '/50.png'}">
                <div class="content">
                    <div class="header" style="color:white"><a>${actividad.memberCreator.fullName}</a> ${new Date(actividad.date).toLocaleString()}</div>
                    <div class="description" style="color:white">
                        <div class="ui compact segment">
                            <p>${mensaje}</p>
                        </div>
                    </div>
                </div>
                </div>`;
            }else{
                actividadesHtml += `<div class="item">
                <img class="ui avatar image" src="${actividad.memberCreator.avatarUrl}/50.png">
                <div class="content">
                        <div class="header" style="color:white"><a>${actividad.memberCreator.fullName}</a> ${new Date(actividad.date).toLocaleString()}</div>
                        <div class="description" style="color:white">${mensaje}</div>
                </div>
                </div>`;   
            }
        }
        return actividadesHtml;
    }

    getMiembros(miembros: Array<Object>){
        let miembrosHtml: string = miembros.length > 0 ? `<div class="four wide column"><h3>Miembros</h3>` : '';
        for(let miembro of miembros){
            miembrosHtml += `<img class="ui avatar image" src="${miembro.avatarUrl}/30.png">`;
        }
        miembrosHtml += miembros.length > 0 ? `</div>` : '';
        return miembrosHtml;
    }

    getListas(listas: Array<{id: string, name: string}>){
        let listasHtml: string = listas.length > 0 ? `<select id="select" onchange="cambiarLista()">` : '';
        for(let lista of listas){
            listasHtml += `<option value=${lista.id}> ${lista.name}</option>`;
        }
        listasHtml += listas.length > 0 ? `</select>` : '';
        return listasHtml;
    }

    getEtiquetas(etiquietas: Array<Object>){
        let etiquetasHtml: string = etiquietas.length > 0 ? `<div class="twelve wide column"><h3>Etiquetas</h3>` : '';
        for(let etiqueta of etiquietas){
            etiquetasHtml += `<a class="ui ${etiqueta.color} label">${etiqueta.name}</a>`;
        }
        etiquetasHtml += etiquietas.length > 0 ? `</div>` : '';
        return etiquetasHtml;
    }

    crearStatusItem(tarjeta: Tarjeta){
        this.tarjetaAsociadaStatus.command = 'trello.abrirTarjetaDesdeStatus';
        this.tarjetaAsociadaStatus.text = `Tarjeta Asociada: ${tarjeta.nombre}`;
        this.tarjetaAsociadaStatus.show();
    }

    borrarStatusItem(){
        this.tarjetaAsociadaStatus.hide();
    }

    asociarArchivo(documento: vscode.TextDocument){
        let carpetaRuta = vscode.workspace.workspaceFolders ? vscode.workspace.workspaceFolders[0].uri.path : undefined ;
        //Permitimos agregar el archivo SOLO si tenemos tarjeta asociada Y si el archivo pertenece a la carpeta actual
        if(this.tarjeta && carpetaRuta && documento.uri.path.includes(carpetaRuta)){
            let nombreArchivo = documento.uri.path.slice(carpetaRuta.length + 1);
            if (! this.archivosAsociados.find((cadena) => cadena === nombreArchivo)){
                this.archivosAsociados.push(nombreArchivo);
                this.archivoProvider = new FileSystemProvider(this.archivosAsociados);
                this.viewArchivo = vscode.window.createTreeView('viewArchivo', {
                    treeDataProvider: this.archivoProvider,
                    showCollapseAll: false
                });
                let soloDescripcion: string = "";
                fs.writeFileSync(this.rutaTarjeta, nombreArchivo + '\n', { flag: 'a+' });
                // traerme la descripcion
                this.cliente.get_tarjeta(this.tarjeta.id).then((respuesta)=>{
                    // editar la descripcion
                    if (respuesta.data.desc.includes("Archivos Asociados:")){
                        let posicion = respuesta.data.desc.indexOf("Archivos Asociados:");
                        soloDescripcion = respuesta.data.desc.slice(0, posicion);
                        soloDescripcion = soloDescripcion + "Archivos Asociados:\n\n";
                    } else {
                        soloDescripcion = respuesta.data.desc + "\n\nArchivos Asociados:\n\n";
                    }
                    for(let archivo of this.archivosAsociados){
                        soloDescripcion = soloDescripcion + "- " + archivo + "\n";
                    }
                    return soloDescripcion;
                }).then((nuevaDescripcion)=>{
                    this.cliente.actualizarTarjeta({'id':this.tarjeta.id,'desc': nuevaDescripcion});
                });
            } else {
                console.log(`el archivo ${nombreArchivo} ya se encuentra asociado`);
            }
        } else {
            if(this.tarjeta) vscode.window.showInformationMessage("Archivo no pertenece al workspace actual");            
        }
    }

    actualizarVistaArchivos(){
        //generamos el nuevo dataprovider con el nuevo archivo asociado
        this.archivoProvider = new FileSystemProvider(this.archivosAsociados);
        this.viewArchivo = vscode.window.createTreeView('viewArchivo', {
            treeDataProvider: this.archivoProvider,
            showCollapseAll: false
        });
    }
}