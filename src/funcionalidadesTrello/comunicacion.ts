// The command has been defined in the package.json file
// Now provide the implementation of the command with registerCommand
// The commandId parameter must match the command field in package.json
import * as vscode from 'vscode';
import axios from 'axios';

let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
    // The code you place here will be executed every time your command is executed
    axios.get('https://api.trello.com/1/boards/57ace25c4af96f21b247cc23?key=a08a78eba567c2e40bc375a85c38e916&token=829599027ec9f35c974af18f88094bafe46034a214dde6a5119bcccac106a784')
    .then(function (response){
        // Display a message box to the user
        vscode.window.showErrorMessage(response.data.id);
    });
    axios.put('https://api.trello.com/1/cards/57ace25c4af96f21b247cc23',
    {
        desc:'prueba con el negrito, hoy no hay gol de messi',
        key:'a08a78eba567c2e40bc375a85c38e916',
        token:'829599027ec9f35c974af18f88094bafe46034a214dde6a5119bcccac106a784'
    })
    .then(r => console.log(r.data))
    .catch(e => console.log(e));
});

//context.subscriptions.push(disposable);